variable "REGION" {
  type        = string
  description = "AWS Region"
}

variable "AWS_ROLE_ARN" {
  type        = string
  description = "AWS Role ARN"
}

variable "PROJECT_NAME" {
  type        = string
  description = "Project name"
}

variable "REPOSITORY_NAME" {
  type        = string
  description = "Repository name"
}

variable "ENVIRONMENT" {
  type        = string
  description = "Environment"
}

variable "CUSTOM_CERTIFICATE_ARN" {
  type        = string
  description = "Custom certificate ARN"
  default     = ""
}

variable "IBISDEV_CERTIFICATE_ARN" {
  type        = string
  description = "IbisDev certificate ARN"
}

variable "CUSTOM_ALIASES" {
  type        = string
  description = "Comma-separated custom aliases"
  default     = ""
}
