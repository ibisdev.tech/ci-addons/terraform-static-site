locals {
  prefix              = "${var.PROJECT_NAME}-${var.REPOSITORY_NAME}"
  bucket_name         = substr("${local.prefix}-${var.ENVIRONMENT}", 0, 36)
  ibisdev_subdomain   = "${local.prefix}${var.ENVIRONMENT == "production" ? "" : "-${var.ENVIRONMENT}"}"
  ibisdev_domain      = "${local.ibisdev_subdomain}.ibisdev.tech"
  custom_aliases      = toset(split(",", var.CUSTOM_ALIASES))
  is_production_ready = var.ENVIRONMENT == "production" && var.CUSTOM_CERTIFICATE_ARN != "" && var.CUSTOM_ALIASES != ""
}
