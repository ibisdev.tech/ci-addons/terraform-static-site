module "cloudfront_distribution" {
  source = "terraform-aws-modules/cloudfront/aws"

  comment             = module.s3_bucket.s3_bucket_id
  price_class         = "PriceClass_All"
  wait_for_deployment = false
  default_root_object = "index.html"

  aliases = local.is_production_ready ? local.custom_aliases : [local.ibisdev_domain]

  origin = {
    s3_website = {
      domain_name = module.s3_bucket.s3_bucket_website_endpoint
      custom_header = [
        {
          name  = "Referer"
          value = random_password.referer.result
        }
      ]
      custom_origin_config = {
        http_port              = 80
        https_port             = 443
        origin_protocol_policy = "http-only"
        origin_ssl_protocols   = ["TLSv1", "TLSv1.1", "TLSv1.2"]
      }
      origin_shield = {
        enabled              = true
        origin_shield_region = var.REGION
      }
    }
  }

  default_cache_behavior = {
    target_origin_id       = "s3_website"
    viewer_protocol_policy = "redirect-to-https"
    use_forwarded_values   = false

    cache_policy_id            = "658327ea-f89d-4fab-a63d-7e88639e58f6"
    origin_request_policy_id   = "88a5eaf4-2fd4-4709-b370-b4c650ea3fcf"
    response_headers_policy_id = "5cc3b908-e619-4b99-88e5-2cf7f45965bd"

    allowed_methods = ["GET", "HEAD", "OPTIONS"]
    cached_methods  = ["GET", "HEAD"]
    compress        = true
  }

  viewer_certificate = {
    acm_certificate_arn = local.is_production_ready ? var.CUSTOM_CERTIFICATE_ARN : var.IBISDEV_CERTIFICATE_ARN
    ssl_support_method  = "sni-only"
  }
}
