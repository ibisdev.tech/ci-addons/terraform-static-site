module "records" {
  providers = {
    aws = aws.ibisdev
  }

  source = "terraform-aws-modules/route53/aws//modules/records"

  count = local.is_production_ready ? 0 : 1

  zone_name = "ibisdev.tech"

  records = [{
    name    = local.ibisdev_subdomain
    type    = "CNAME"
    ttl     = 60
    records = [module.cloudfront_distribution.cloudfront_distribution_domain_name]
  }]
}
